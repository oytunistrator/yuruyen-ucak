let type = "WebGL"
if (!PIXI.utils.isWebGLSupported()) {
    type = "canvas"
}

PIXI.utils.sayHello = function() {};

let app = new PIXI.Application({ wdth: 600, heigth: 400 });

PIXI.loader
    .add("img/ucak1.png")
    .add("img/ucak2.png")
    .add("img/ucak3.png")
    .add("bg", "img/bg.jpg")
    .add("bullet", "img/bullet.png")
    .on("progress", loadProgressHandler)
    .load(setup);

function loadProgressHandler(loader, resource) {

}

let ucak;
let bg;
let scoreText;
let statusText;
let totalScore = 0;
let circle = new PIXI.Graphics();
let stage = new PIXI.Container();

stage.interactive = true;


function keyboard(keyCode) {
    let key = {};
    key.code = keyCode;
    key.isDown = false;
    key.isUp = true;
    key.press = undefined;
    key.release = undefined;
    //The `downHandler`
    key.downHandler = event => {
        if (event.keyCode === key.code) {
            if (key.isUp && key.press) key.press();
            key.isDown = true;
            key.isUp = false;
        }
        event.preventDefault();
    };

    //The `upHandler`
    key.upHandler = event => {
        if (event.keyCode === key.code) {
            if (key.isDown && key.release) key.release();
            key.isDown = false;
            key.isUp = true;
        }
        event.preventDefault();
    };

    //Attach event listeners
    window.addEventListener(
        "keydown", key.downHandler.bind(key), false
    );
    window.addEventListener(
        "keyup", key.upHandler.bind(key), false
    );
    return key;
}


function hitTestRectangle(r1, r2) {

    //Define the variables we'll need to calculate
    let hit, combinedHalfWidths, combinedHalfHeights, vx, vy;

    //hit will determine whether there's a collision
    hit = false;

    //Find the center points of each sprite
    r1.centerX = r1.x + r1.width / 2;
    r1.centerY = r1.y + r1.height / 2;
    r2.centerX = r2.x + r2.width / 2;
    r2.centerY = r2.y + r2.height / 2;

    //Find the half-widths and half-heights of each sprite
    r1.halfWidth = r1.width / 2;
    r1.halfHeight = r1.height / 2;
    r2.halfWidth = r2.width / 2;
    r2.halfHeight = r2.height / 2;

    //Calculate the distance vector between the sprites
    vx = r1.centerX - r2.centerX;
    vy = r1.centerY - r2.centerY;

    //Figure out the combined half-widths and half-heights
    combinedHalfWidths = r1.halfWidth + r2.halfWidth;
    combinedHalfHeights = r1.halfHeight + r2.halfHeight;

    //Check for a collision on the x axis
    if (Math.abs(vx) < combinedHalfWidths) {

        //A collision might be occuring. Check for a collision on the y axis
        if (Math.abs(vy) <= combinedHalfHeights) {

            //There's definitely a collision happening
            hit = true;
        } else {

            //There's no collision on the y axis
            hit = false;
        }
    } else {

        //There's no collision on the x axis
        hit = false;
    }

    //`hit` will be either `true` or `false`
    return hit;
};

function score(status, count) {
    switch (status) {
        case "add":
            totalScore = totalScore + count;
            break;
        case "remove":
            totalScore = totalScore - count;
            break;
    }
    scoreText.text = "SKOR :" + totalScore;
    return;
}

function setup() {
    var frames = [];

    for (var i = 1; i <= 3; i++) {
        var val = i < 10 ? '' + i : i;

        // magically works since the spritesheet was loaded with the pixi loader
        frames.push(PIXI.Texture.fromFrame('img/ucak' + val + '.png'));
    }
    bg = new PIXI.Sprite(PIXI.loader.resources.bg.texture);
    ucak = new PIXI.extras.MovieClip(frames);
    ucak.animationSpeed = 0.5;
    ucak.play();

    let scoreStyle = new PIXI.Text({
        fontFamily: "Arial",
        fontSize: 36,
        fill: "white",
        stroke: '#fff',
        strokeThickness: 4,
        dropShadow: true,
        dropShadowColor: "#000000",
        dropShadowBlur: 4,
        dropShadowAngle: Math.PI / 6,
        dropShadowDistance: 6,
    });
    scoreText = new PIXI.Text("SKOR :" + totalScore, scoreStyle);
    statusText = new PIXI.Text("", scoreStyle);
    statusText.position.set((app.width / 2), 30);
    scoreText.position.set((app.width / 2), 0);

    ucak.x = 10;
    ucak.y = 400;
    ucak.width = 70;
    ucak.height = 30;
    ucak.vx = 0;
    ucak.vy = 0;
    ucak.animationSpeed = 0.5;



    circle.beginFill(0x000);
    circle.drawCircle(0, 0, 32);
    circle.endFill();
    circle.x = 600;
    circle.y = 400;
    circle.width = 100;
    circle.height = 100;



    app.stage.addChild(bg);

    app.stage.addChild(circle);
    app.stage.addChild(ucak);
    app.stage.addChild(scoreText);
    app.stage.addChild(statusText);




    let left = keyboard(37),
        up = keyboard(38),
        right = keyboard(39),
        down = keyboard(40),
        space = keyboard(32);

    left.press = () => {
        ucak.vx = -5 * ucak.animationSpeed;
        ucak.vy = 0;
    };

    left.release = () => {
        if (!right.isDown && ucak.vy === 0) {
            ucak.vx = 0;

        }
    };

    up.press = () => {
        ucak.vy = -5 * ucak.animationSpeed;
        ucak.vx = 0;
    }
    up.release = () => {
        if (!down.isDown && ucak.vx === 0) {
            ucak.vy = 0;
        }
    };

    //Right
    right.press = () => {
        ucak.vx = 5 * ucak.animationSpeed;
        ucak.vy = 0;
    };
    right.release = () => {
        if (!left.isDown && ucak.vy === 0) {
            ucak.vx = 0;
        }
    };

    //Down
    down.press = () => {
        ucak.vy = 5 * ucak.animationSpeed;
        ucak.vx = 0;
    };
    down.release = () => {
        if (!up.isDown && ucak.vx === 0) {
            ucak.vy = 0;

        }
    };

    space.press = () => {
        shoot(ucak.rotation, {
            x: ucak.position.x + Math.cos(ucak.rotation) * 20,
            y: ucak.position.y + Math.sin(ucak.rotation) * 20
        });
    }
    space.release = () => {

    }

    //Set the game state
    state = play;

    //Start the game loop 
    app.ticker.add(delta => gameLoop(delta));
}

let bullets = [];
let bulletSpeed = 5;

function shoot(rotation, startPosition) {
    var bullet = new PIXI.Sprite(PIXI.loader.resources.bullet.texture);
    bullet.position.x = startPosition.x;
    bullet.position.y = startPosition.y - 30;
    bullet.width = 90;
    bullet.height = 100;
    bullet.rotation = rotation;
    app.stage.addChild(bullet);
    bullets.push(bullet);
}

function gameLoop(delta) {
    state(delta);
}

function play(delta) {
    ucak.x += ucak.vx;
    ucak.y += ucak.vy;

    circle.position.y += Math.sin(circle.position.y + 100) * bulletSpeed;
    circle.position.y -= Math.sin(circle.position.y + 100) * bulletSpeed;


    for (var b = bullets.length - 1; b >= 0; b--) {
        bullets[b].position.x += Math.cos(bullets[b].rotation) * bulletSpeed;
        bullets[b].position.y += Math.sin(bullets[b].rotation) * bulletSpeed;

        if (hitTestRectangle(bullets[b], circle)) {
            circle.tint = 0xfff;
            score("add", 10);
        } else {
            circle.tint = 0x000;
        }
    }


}

document.body.appendChild(app.view)