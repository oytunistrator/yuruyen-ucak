local anim8 = require 'anim8'
local image, animation

score = 0

player = { x = 10, y = 400, speed = 100, img = nil, width = 370, heigth = 200 }
background = nil

function scoreChange(option, val)
    if score < 0 then
        score = 0
    else
        if option == "add" then
            score = score + val
        elseif option == "remove" then
            score = score - val
        end
    end
end

function checkPlayer(x, y)
    if player.x == x and player.y == y then
        return true
    end
    return false
end

function returnPlayer(dt, side)
    if side == "left" then
        if player.x < (0 - player.width) then
            player.x = 0 - (player.speed*dt) 
        else
            player.x = player.x - (player.speed*dt)
        end
        
    elseif side == "right" then
        if player.x >= (600 + player.width) then
            player.x = (600 + player.width) + (player.speed*dt)
        else
            player.x = player.x + (player.speed*dt)
        end
    end
end

function love.load(arg)
    player.img = love.graphics.newImage('img/ucak-sprite.png')
  local g = anim8.newGrid(player.width, player.heigth, player.img:getWidth(), player.img:getHeight())
  animation = anim8.newAnimation(g('1-3',1), 0.1)
  background = love.graphics.newImage("img/bg.jpg")
end

function love.update(dt)
    if love.keyboard.isDown('escape') then
		love.event.push('quit')
    end
    if love.keyboard.isDown('up','w') then
        player.y = player.y - (player.speed*dt)
        scoreChange('add', 1)
		
    elseif love.keyboard.isDown('down','s') then
        if player.y >= 400 then
            player.y = 400 + (player.speed*dt)
        else
            player.y = player.y + (player.speed*dt)
            scoreChange('remove', 1)
        end
    end
    if love.keyboard.isDown('right','d') then
        returnPlayer(dt, "right")
        
        scoreChange('add', 1)
    elseif love.keyboard.isDown('left', 'a') then
        returnPlayer(dt, "left")
        scoreChange('remove', 1)
	end
    animation:update(dt)
end

function love.draw(dt)
    
    
    for i = 0, love.graphics.getWidth() / background:getWidth() do
        for j = 0, love.graphics.getHeight() / background:getHeight() do
            love.graphics.draw(background, i * background:getWidth(), j * background:getHeight())
        end
    end

    love.graphics.setColor(255, 255, 255)
    love.graphics.print("SCORE: " .. tostring(score), 400, 10)

    animation:draw(player.img, player.x, player.y)
end