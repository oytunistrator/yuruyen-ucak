-- Configuration
function love.conf(t)
	t.title = "Yürüyen Uçak" -- The title of the window the game is in (string)
	t.window.width = 800        -- we want our game to be long and thin.
	t.window.height = 600

	-- For Windows debugging
	t.console = true
end